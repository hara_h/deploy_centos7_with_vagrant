# deploy_centos7_with_vagrant

virtualbox + vagrant で検証用LinuxとしてCentOS7を立てるだけの簡単な作業です。  

## 事前インストール項目

- virtualbox
- vagrant
- teraterm
    - 以下リンクより公式サイトへアクセスしダウンロードしてインストールしてください。  
      [virtualbox](https://www.virtualbox.org/)  
      [vagrant](https://www.vagrantup.com/)  
      [teraterm](https://forest.watch.impress.co.jp/library/software/utf8teraterm/)  

■e-wordよりソフトウェアの説明
- [virtualboxとは？](https://e-words.jp/w/VirtualBox.html)
- [vagrantとは？](https://e-words.jp/w/Vagrant.html)
- [teratermとは？](https://e-words.jp/w/Tera_Term.html)  
どんなソフトなのかはとりあえず使ってみましょう。あとはググって調べるべし！

## 作業内容

1. 本プロジェクトのダウンロードをzip等で行い、適当なフォルダに展開します。 
    ![image](/uploads/8ebaa5cc375650fdb5b96a8141ef976e/image.png)  
     
    展開データの中身のCentOS7フォルダをCドライブ直下等のアルファベットのみで構成されるフォルダに保存してください。  
    【CentOS7フォルダの保存例】  
    ![image](/uploads/009db768d9cca1ef59f7063569c1d66a/image.png)

2. コマンドプロンプトを開きます。  
    Windowsシステムツールから探してもいいですが、アプリケーションの検索で「cmd」と打つと早いです。
    ![image](/uploads/53d9da3428e2e81a0d53e3f3a98429b6/image.png)

3. cdコマンドを利用しCentOS7フォルダに移動します。  
    まずは保存したフォルダのフルパスを確認します。
    ![image](/uploads/6e1865c36cc7f3b58d198128f4a3f1d8/image.png)  

    cdコマンドでcentos7フォルダに移動しましょう。  
    【例】
    ```
    cd c:\centos7
    ```

4. vagrantを利用してCentOS7を起動します。以下コマンドを実行してください。  
    ### 【最初にお願い】Windowsユーザー名が日本語になっている方に向けて  
    ```VAGRANT_HOME```という環境変数を日本語名がフルパス上で含まれない場所に変更します。  
    【例】  
    ```
    set VAGRANT_HOME="c:\CentOS7\box\vagrant.d\"
    ```

    ### ここからメインコマンド
    ```
    vagrant up
    ```
    起動が完了すると```default: Complete!```の文字が現れます。
    
5. centos7へ接続しましょう。以下コマンドを実行してssh接続を行います。
    ```
    vagrant ssh
    ```
    - ※ teratermで仮想マシンにログインする方法
      teratermを利用すると自分の行ったコマンドやその出力結果を簡単にログに残すことができます。(逆に "vagrant ssh" でログを残すことがまずできない…)

      1. teratermを開いて、以下IPアドレスを入力する。  
          ```192.168.56.200```  
          ![image](/uploads/e30bfa51f450a5940f78521eb4e0fea1/image.png)

      2. ユーザー名、パスワードをともに「vagrant」を入力、Autentication Methodで「RSA/DSA/ECDSA/ED25519鍵を使う」を選択します。  
          ![image](/uploads/9066d4b68a4aed9316ba97eca9a80957/image.png)

      3. 秘密鍵を選択します。```vagrant up```を実行した際のフォルダ配下に出来上がった```.vagrant\machines\default\virtualbox```内の```private_key```を選択します。
          【例】
          ![image](/uploads/531dafc40633dd9bf036661b197188b4/image.png)

      4. 「OK」を押下して接続します。
        


6. ※これは作業時のお願いです。
    実際に作業を行うにあたって、変更作業等を伴う場合はスナップショットというその時の状態を保存しておくデータを取得しておきましょう。
    ```
    # スナップショット取得コマンド
    vagrant snapshot save <任意の名前>

    # スナップショットのリスト確認
    vagrant snapsot list 

    # スナップショットからの復元(リストア)
    vagrant snapshot restore <スナップショット名>

    # スナップショットの削除
    vagrant snapshot delete <スナップショット名>
    ```
    ゲームのセーブポイントと同じ感じですね。


■ vagrant簡易コマンドリスト  
| コマンド        | 内容                         | 
| --------------- | ---------------------------- | 
| vagrant up      | 仮想マシンの起動             | 
| vagrant halt    | 仮想マシンのシャットダウン   | 
| vagrant suspend | 仮想マシンの一時停止         | 
| vagrant resume  | 仮想マシンの一時停止から復帰 | 
| vagrant reload  | 仮想マシンの再起動           | 
| vagrant destroy | 仮想マシンの削除             | 
| vagrant status  | 仮想マシンの状態確認         | 

■ Vagrantの起動に失敗したり、仮想マシンの挙動がおかしくなったら…  
上記コマンドを駆使して作成しなおしたり、スナップショットから復元して正常に動作していた状態に戻しましょう。  

- 仮想マシンを完全に削除する際に
  ```vagrant destroy```の実行の他、作成されたvirtualBoxのデータを合わせて削除しましょう。  
  virutalBoxの仮想マシンのデータはデフォルトでは以下に保存されます。該当フォルダ内のデータを削除しておきましょう。    
  ```C:\Users\[ユーザ名]\VirtualBox VMs```  
  【例】私はDドライブに保存場所を作りましたのでそちらを削除する形
    ![image](/uploads/b25a71ed62176695b16ace0be6be9fd3/image.png)

